/// <reference path="../../typings.d.ts" />

import * as express from 'express';
import * as crypto from 'crypto';
import * as moment from 'moment';
import * as _ from 'lodash';

import { Router, Request, Response } from 'express';
import * as HttpStatus from 'http-status-codes';
// import model
// import { TestModel } from "../models/test";
import { JwtModel } from "../models/jwt";
import { UserModel } from '../models/user';
import { LineModel } from '../models/line';
import { SurveilModel } from '../models/surveil';

// const testModel = new TestModel();
const userModel = new UserModel();
const jwtModel = new JwtModel();
const lineModel = new LineModel();
const surveilModel = new SurveilModel();

const router: Router = Router();

router.get('/', async (req: Request, res: Response) => {
  var token = jwtModel.sign({ hello: 'xxx' });
  console.log(token);
  res.send({ ok: true, message: 'Welcome to RESTful api server!', code: HttpStatus.OK });
});

router.post('/webhook', async (req: Request, res: Response) => {
  console.log(req.body.events);
  if (req.body.events.length) {
    if (req.body.events[0].type === 'message') {
      const replyToken = req.body.events[0].replyToken;
      const lineId = req.body.events[0].source.userId;
      const message = req.body.events[0].message.type === 'text' ? req.body.events[0].message.text : '';

      if (message === 'register') {
        console.log(message);
        const _linkMsg = `${process.env.REGISTER_LINK}?lineId=${lineId}`;

        const register: any = {
          "type": "template",
          "altText": "This is a buttons template",
          "template": {
            "type": "buttons",
            "title": "ลงทะเบียน",
            "text": "กรุณาคลิกลงทะเบียน",
            "actions": [
              {
                "type": "uri",
                "label": "ลงทะเบียน",
                "uri": _linkMsg
              }]
          }
        };

        lineModel.replyMessage(replyToken, [register]);
      } else {
        console.log('xxxx');
      }
    }
  }
  res.sendStatus(200);
});

router.post('/login', async (req: Request, res: Response) => {

  const db = req.db;

  const username = req.body.username;
  const password = req.body.password;

  if (username && password) {
    try {
      const encPassword = crypto.createHash('md5').update(password).digest('hex');

      const rs: any = await userModel.doLogin(db, username, encPassword);
      if (rs.length) {
        const user = rs[0];
        const info: any = {};
        info.id = user.id;
        info.level = user.level;
        info.hospcode = user.hospcode;
        info.areaCode = user.amphur;

        const token = jwtModel.sign(info);
        res.send({ ok: true, token: token });
      } else {
        res.send({ ok: false, error: 'ชื่อผู้ใช้งาน/รหัสผ่าน ไม่ถูกต้อง' });
      }
    } catch (error) {
      console.log(error);
      res.send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  } else {
    res.send({ ok: false, error: 'ข้อมูลไม่ครบ' })
  }

});

router.post('/verify-line', async (req: Request, res: Response) => {

  const db = req.db;

  const lineId = req.body.lineId;
  const accessToken = req.body.accessToken;

  try {

    /// verify access token

    const isValidToken: any = await lineModel.verifyAccessToken(accessToken);

    const decoded = JSON.parse(isValidToken);

    if (_.has(decoded, 'client_id')) {
      const rs: any = await userModel.checkRegisteredLine(db, lineId);

      if (rs.length > 0) {
        const user = rs[0];
        const info: any = {};
        info.id = user.id;
        info.level = user.level;
        info.hospcode = user.hospcode;
        info.areaCode = user.amphur;

        const token = jwtModel.sign(info);
        res.send({ ok: true, token: token });
      } else {
        res.send({ ok: false, error: 'ไม่มีข้อมูล LINE ในระบบ' });
      }
    } else {
      res.send({ ok: false, error: 'Invalid LINE access token' });
    }

  } catch (error) {
    console.log(error);
    res.send({ ok: false, error: 'เกิดข้อผิดพลาด' });
  }

});

router.post('/login-line', async (req: Request, res: Response) => {

  const db = req.db;

  const username = req.body.username;
  const password = req.body.password;
  const lineId = req.body.lineId;

  if (username && password) {
    try {
      const encPassword = crypto.createHash('md5').update(password).digest('hex');

      const rs: any = await userModel.doLogin(db, username, encPassword);
      if (rs.length) {
        await userModel.saveLineId(db, username, lineId);
        res.render('login-line-success', { message: 'คุณได้ลงทะเบียนในระบบแล้ว' });
      } else {
        res.render('login-line-error', { message: 'ไม่สามารถเข้าสู่ระบบได้' });
      }
    } catch (error) {
      console.log(error);
      res.send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  } else {
    res.send({ ok: false, error: 'ข้อมูลไม่ครบ' })
  }

});

router.get('/line-login', async (req: Request, res: Response) => {
  const lineId = req.query.lineId;
  try {
    const rs: any = await userModel.checkRegisteredLine(req.db, lineId);
    // console.log(lineId);
    // console.log(rs);

    if (rs.length) {
      res.render('login-line-success', { message: 'คุณได้ลงทะเบียนในระบบแล้ว' });
    } else {
      res.render('login-line', { lineId: lineId });
    }
  } catch (error) {
    res.render('login-line-error', {message: error.message});
  }
});

router.get('/line/accept', async (req: Request, res: Response) => {
  const idLine = req.query.idLine;
  const userId = req.query.userId;
  const hospcode = req.query.hospcode;
  const surveilId = req.query.surveilId;

  const db = req.db;
  // check status
  try {
    const rs: any = await surveilModel.checkReplyStatus(db, surveilId, hospcode);
    if (rs.length) {
      res.render('login-line-error', { message: 'ข้อมูลได้รับการยืนยันไปแล้ว โดย ' + rs[0].user_fullname });
    } else {
      const responseTime = moment().format('YYYY-MM-DD HH:mm:ss');
      await surveilModel.updateReplyStatus(db, surveilId, 'Y', hospcode, userId, responseTime);

      // serveil info
      const rsLogInfo: any = await surveilModel.checkSendLogInfo(db, surveilId);
      const userSendId: any = rsLogInfo.length ? rsLogInfo[0].user_send : null;

      // get user info
      const rsUserInfo: any = await userModel.getUserInfo(db, userSendId);

      if (rsUserInfo.length) {
        if (rsUserInfo[0].id_line) {
          const _msg: any = {
            type: 'text',
            text: `ข้อมูลระบาด ${rsLogInfo[0].pt_name} ได้รับการตอบรับจากเจ้าหน้าที่แล้ว`
          };

          const lineId = rsUserInfo[0].id_line;

          lineModel.pushMessage(lineId, [_msg]);
        }
      }

      res.render('login-line-success', { message: 'บันทึกข้อมูลเรียบร้อย' });


    }
  } catch (error) {
    res.send({ ok: false, error: error.message });
    console.log(error);
  }

});

router.post('/ready-send', async (req: Request, res: Response) => {
  const hospcode = req.body.hospcode;
  const db: any = req.db;

  try {
    const rs: any = await surveilModel.getReadySend(db, hospcode);
    res.send({ ok: true, rows: rs });
  } catch (error) {
    res.send({ ok: false, error: 'เกิดข้อผิดพลาด [500]' })
  }
});

export default router;
