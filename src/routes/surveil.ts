/// <reference path="../../typings.d.ts" />

import * as express from 'express';
import * as crypto from 'crypto';
import * as moment from 'moment';

import { Router, Request, Response } from 'express';
import * as HttpStatus from 'http-status-codes';

import { SurveilModel } from '../models/surveil';
import { UserModel } from '../models/user';
import { LineModel } from '../models/line';

const surveilModel = new SurveilModel();
const userModel = new UserModel();
const lineModel = new LineModel();

const router: Router = Router();

router.get('/send-history', async (req: Request, res: Response) => {
  const hospcode = req.decoded.hospcode;
  const db: any = req.db;
  const query = req.query.query;

  const limit = +req.query.limit || 20;
  const offset = +req.query.offset || 0;

  try {
    const rs: any = await surveilModel.getHistory(db, hospcode, query, limit, offset);
    const total = await surveilModel.getHistoryTotal(db, hospcode, query);

    const items = rs.map(v => {
      v.vstdate = `${moment(v.vstdate).locale('th').format('D MMM')} ${moment(v.vstdate).get('year') + 543}`;
      v.eng_date = v.vstdate;
      return v;
    });

    res.send({ ok: true, rows: items, total });
  } catch (error) {
    res.send({ ok: false, error: 'เกิดข้อผิดพลาด [500]' })
  }
});

router.get('/history', async (req: Request, res: Response) => {
  const hospcode = req.decoded.hospcode;
  const db: any = req.db;
  const query = req.query.query;

  try {
    const rs: any = await surveilModel.getHistoryList(db, hospcode, query);
    const items = rs.map(v => {
      v.vstdate = `${moment(v.vstdate).locale('th').format('D MMM')} ${moment(v.vstdate).get('year') + 543}`;
      v.eng_date = v.vstdate;
      return v;
    });

    res.send({ ok: true, rows: items });
  } catch (error) {
    res.send({ ok: false, error: 'เกิดข้อผิดพลาด [500]' })
  }
});

router.get('/job/history', async (req: Request, res: Response) => {
  const hospcode = req.decoded.hospcode;
  const db: any = req.db;
  const query = req.query.query;

  try {
    const rs: any = await surveilModel.getJobHistoryList(db, hospcode, query);
    const items = rs.map(v => {
      v.vstdate = `${moment(v.vstdate).locale('th').format('D MMM')} ${moment(v.vstdate).get('year') + 543}`;
      v.eng_date = v.vstdate;
      return v;
    });

    res.send({ ok: true, rows: items });
  } catch (error) {
    res.send({ ok: false, error: 'เกิดข้อผิดพลาด [500]' })
  }
});

router.get('/user-info', async (req: Request, res: Response) => {
  const userId = req.decoded.id;
  const db: any = req.db;

  try {
    const rs: any = await userModel.getInfo(db, userId);
    res.send({ ok: true, info: rs[0] });
  } catch (error) {
    res.send({ ok: false, error: 'เกิดข้อผิดพลาด [500]' })
  }
});

router.post('/line-info', async (req: Request, res: Response) => {
  const userId = req.decoded.id;

  const lineName = req.body.lineName;
  const lineEmail = req.body.lineEmail;
  const lineId = req.body.lineId;
  const linePictureUrl = req.body.linePictureUrl;

  const db: any = req.db;

  try {
    await userModel.saveLineInfo(db, userId, lineId, lineName, lineEmail, linePictureUrl);
    res.send({ ok: true });
  } catch (error) {
    console.log(error);
    res.send({ ok: false, error: 'เกิดข้อผิดพลาด [500]' })
  }
});

router.post('/prevalence-radius', async (req: Request, res: Response) => {
  const surveil_id = req.body.surveilId;
  const address = req.body.address;
  const house_in_container_radius = req.body.houseInContainerRadius;
  const house_out_container_radius = req.body.houseOutContainerRadius;
  const house_out_mosquito_radius = req.body.houseOutMosquitoRadius;
  const house_in_mosquito_radius = req.body.houseInMosquitoRadius;
  const period_day = req.body.periodDay;

  const db: any = req.db;

  try {

    const rsCheck: any = await surveilModel.checkPrevalanceRadiusHouse(db, surveil_id, address);
    if (rsCheck.length > 0) {
      res.send({ ok: false, error: 'ข้อมูลซ้ำ' });
    } else {
      const data = {
        surveil_id,
        address,
        house_in_container_radius,
        house_out_container_radius,
        house_out_mosquito_radius,
        house_in_mosquito_radius,
        period_day
      };
      await surveilModel.savePrevalanceRadius(db, data);
      res.send({ ok: true });
    }

  } catch (error) {
    console.log(error);
    res.send({ ok: false, error: 'เกิดข้อผิดพลาด [500]' })
  }
});

router.get('/prevalence-radius/:periodDay', async (req: Request, res: Response) => {

  const periodDay = req.params.periodDay;

  const db: any = req.db;

  try {
    const rs: any = await surveilModel.getPrevalanceRadius(db, periodDay);
    const items = rs.map(v => {
      v.date_update = `${moment(v.date_update).locale('th').format('D MMM')} ${moment(v.date_update).get('year') + 543} ${moment(v.date_update).format('HH:mm')} น.`;
      return v;
    })
    res.send({ ok: true, rows: items });
  } catch (error) {
    console.log(error);
    res.send({ ok: false, error: 'เกิดข้อผิดพลาด [500]' })
  }
});

router.delete('/prevalence-radius/:prevalenceRadiusId', async (req: Request, res: Response) => {

  const prevalenceRadiusId = req.params.prevalenceRadiusId;

  const db: any = req.db;

  try {
    await surveilModel.removePrevalanceRadius(db, prevalenceRadiusId);
    res.send({ ok: true });
  } catch (error) {
    console.log(error);
    res.send({ ok: false, error: 'เกิดข้อผิดพลาด [500]' })
  }
});

router.post('/check-prevalence', async (req: Request, res: Response) => {

  const surveilId = req.body.surveilId;

  const db: any = req.db;

  try {
    const rsInfo: any = await surveilModel.getSurveilInfo(db, surveilId);
    const hospname = rsInfo.length > 0 ? rsInfo[0].hospname : 'ไม่ทราบ';
    const hamp = rsInfo.length > 0 ? rsInfo[0].hamp : 'ไม่ทราบ';

    const rsFirst: any = await surveilModel.getFirstDate(db, surveilId);
    const day1 = moment(rsFirst[0].vstdate).add(1, 'day');
    const day3 = moment(rsFirst[0].vstdate).add(3, 'day');
    const day7 = moment(rsFirst[0].vstdate).add(7, 'day');

    const thaiDay1 = `${day1.locale('th').format('DD MMM')} ${day1.get('year') + 543}`;
    const thaiDay3 = `${day3.locale('th').format('DD MMM')} ${day3.get('year') + 543}`;
    const thaiDay7 = `${day7.locale('th').format('DD MMM')} ${day7.get('year') + 543}`;

    const total = await surveilModel.countPrevalence(db, surveilId);
    res.send({ ok: true, total, thaiDay1, thaiDay3, thaiDay7, hospname, hamp });
  } catch (error) {
    console.log(error);
    res.send({ ok: false, error: 'เกิดข้อผิดพลาด [500]' })
  }
});

router.get('/ampur/history', async (req: Request, res: Response) => {

  const hospcode = req.decoded.hospcode;
  const query = req.query.query || null;
  const db: any = req.db;

  try {
    const rs: any = await surveilModel.getHistoryListByAmpur(db, hospcode, query);

    const items = rs.map(v => {
      v.vstdate = `${moment(v.vstdate).locale('th').format('D MMM')} ${moment(v.vstdate).get('year') + 543}`;
      v.eng_date = v.vstdate;
      return v;
    });

    res.send({ ok: true, rows: items });
  } catch (error) {
    console.log(error);
    res.send({ ok: false, error: 'เกิดข้อผิดพลาด [500]' })
  }
});

router.get('/province/history', async (req: Request, res: Response) => {

  const hospcode = req.decoded.hospcode;
  const query = req.query.query || null;
  const db: any = req.db;

  try {
    const rs: any = await surveilModel.getHistoryListByAmpur(db, hospcode, query);

    const items = rs.map(v => {
      v.vstdate = `${moment(v.vstdate).locale('th').format('D MMM')} ${moment(v.vstdate).get('year') + 543}`;
      v.eng_date = v.vstdate;
      return v;
    });

    res.send({ ok: true, rows: items });
  } catch (error) {
    console.log(error);
    res.send({ ok: false, error: 'เกิดข้อผิดพลาด [500]' })
  }
});

router.get('/ampur/clients', async (req: Request, res: Response) => {

  const hospcode = req.decoded.hospcode;
  const db: any = req.db;

  try {
    const rs: any = await surveilModel.getClients(db, hospcode);

    res.send({ ok: true, rows: rs });
  } catch (error) {
    console.log(error);
    res.send({ ok: false, error: 'เกิดข้อผิดพลาด [500]' })
  }
});

router.get('/province/clients', async (req: Request, res: Response) => {

  const db: any = req.db;

  try {
    const rs: any = await surveilModel.getProvinceClients(db, '70');

    res.send({ ok: true, rows: rs });
  } catch (error) {
    console.log(error);
    res.send({ ok: false, error: 'เกิดข้อผิดพลาด [500]' })
  }
});

router.post('/completed', async (req: Request, res: Response) => {

  // const hospcode = req.decoded.hospcode;
  const surveilId = req.body.surveilId;

  const db: any = req.db;

  try {
    await surveilModel.changeSurveilStatus(db, surveilId, 'Z');
    res.send({ ok: true });
  } catch (error) {
    console.log(error);
    res.send({ ok: false, error: 'เกิดข้อผิดพลาด [500]' })
  }
});

router.post('/transfer', async (req: Request, res: Response) => {

  const hamp = req.decoded.hospcode;
  const hsub = req.body.hsub;
  const surveilId = req.body.surveilId;
  const status = req.body.status;

  const db: any = req.db;

  try {
    // get surveil info

    const rs: any = await surveilModel.getSurveilInfo(db, surveilId);

    if (rs.length > 0) {
      const surveil = rs[0];
      const _surveil: any = {};

      _surveil.hospcode = surveil.hospcode;
      _surveil.hn = surveil.hn;
      _surveil.sv_number = surveil.sv_number;
      _surveil.vn = surveil.vn;
      _surveil.pdx = surveil.pdx;
      _surveil.vstdate = surveil.vstdate;
      _surveil.report_date = surveil.report_date;
      _surveil.begin_date = surveil.begin_date;
      _surveil.icd_name = surveil.icd_name;
      _surveil.name506 = surveil.name506;
      _surveil.code506 = surveil.code506;
      _surveil.cc = surveil.cc;
      _surveil.pe = surveil.pe;
      _surveil.ill_addr = surveil.ill_addr;
      _surveil.ill_moo = surveil.ill_moo;
      _surveil.ill_tmbpart = surveil.ill_tmbpart;
      _surveil.ill_amppart = surveil.ill_amppart;
      _surveil.ill_chwpart = surveil.ill_chwpart;
      _surveil.diag_dangue = surveil.diag_dangue;
      _surveil.time_send = moment().format('YYYY-MM-DD HH:mm:ss');
      _surveil.user_id = req.decoded.id;
      _surveil.to = hsub;
      _surveil.from = hamp;

      await surveilModel.saveSurveil(db, _surveil);
      await surveilModel.changeSurveilStatus(db, surveilId, status);
      res.send({ ok: true, });
    } else {
      res.send({ ok: false, error: 'ไม่พบข้อมูล' });

    }
  } catch (error) {
    console.log(error);
    res.send({ ok: false, error: 'เกิดข้อผิดพลาด [500]' })
  }
});

router.post('/prevalence', async (req: Request, res: Response) => {
  const user_id = req.decoded.id;

  const hospcode = req.decoded.hospcode;
  const hn = req.body.hn;
  const vn = req.body.vn;
  const period_day = +req.body.periodDay === 1 ? 1 : +req.body.periodDay === 2 ? 3 : 7;
  const surveil_id = req.body.surveilId;

  const prevalence_date = moment().format('YYYY-MM-DD HH:mm:ss');
  const house_in_container = +req.body.houseInContainer || 0;
  const house_in_mosquito = +req.body.houseInMosquito || 0;
  const house_out_container = +req.body.houseOutContainer || 0;
  const house_out_mosquito = +req.body.houseOutMosquito || 0;

  const house_in_ci = (+house_in_mosquito + +house_out_mosquito) * 100 / (+house_in_container + +house_out_container) || 0;
  const house_out_survey = +req.body.houseOutSurvey || 0;
  const house_out_survey_mosquito = +req.body.houseOutSurveyMosquito || 0;
  const house_out_ci = +req.body.totalRadiusCi || 0;
  const house_out_hi = 0;
  const school_in_container = +req.body.schoolInContainer || 0;
  const school_in_mosquito = +req.body.schoolInMosquito || 0;
  const school_out_container = +req.body.schoolOutContainer || 0;
  const school_out_mosquito = +req.body.schoolOutMosquito || 0;
  const school_ci = (+school_in_mosquito + +school_out_mosquito) * 100 / (+school_in_container + +school_out_container) || 0;
  const temple_container = req.body.templeContainer;
  const temple_mosquito = req.body.templeMosquito;
  const temple_ci = +req.body.templeCi || 0;
  const control_sand = req.body.controlSand;
  const control_sand_total = +req.body.controlSandTotal || 0;
  const control_fish = req.body.controlFish;
  const control_fish_total = +req.body.controlFishTotal || 0;
  const control_other = req.body.controlOther;
  const control_other_total = +req.body.controlOtherTotal || 0;
  const control_source = req.body.controlSource;
  const control_source_total = +req.body.controlSourceTotal || 0;
  const control_chem_smog = req.body.controlChemSmog;
  const control_chem_smog_total = +req.body.controlChemSmogTotal || 0;
  const control_chem_spray = req.body.controlChemSpray;
  const control_chem_spray_total = +req.body.controlChemSprayTotal || 0;
  const control_chem_school = req.body.controlChemSchool;
  const control_chem_school_total = +req.body.controlChemSchoolTotal || 0;
  const control_chem_temple = req.body.controlChemTemple;
  const control_chem_temple_total = +req.body.controlChemTempleTotal || 0;
  const images = req.body.images;
  const latitude = req.body.lat;
  const longtitude = req.body.lng;

  const arrImages = images ? JSON.parse(images) : [];

  const db: any = req.db;

  delete req.body.images;

  console.log(req.body);

  try {
    const data = {
      hospcode,
      hn,
      vn,
      surveil_id,
      period_day,
      latitude,
      longtitude,
      prevalence_date,
      house_in_container,
      house_in_mosquito,
      house_out_container,
      house_out_mosquito,
      house_in_ci,
      house_out_survey,
      house_out_survey_mosquito,
      house_out_ci,
      house_out_hi,
      school_in_container,
      school_in_mosquito,
      school_out_container,
      school_out_mosquito,
      school_ci,
      temple_container,
      temple_mosquito,
      temple_ci,
      control_sand,
      control_sand_total,
      control_fish,
      control_fish_total,
      control_other,
      control_other_total,
      control_source,
      control_source_total,
      control_chem_smog,
      control_chem_smog_total,
      control_chem_spray,
      control_chem_spray_total,
      control_chem_school,
      control_chem_school_total,
      control_chem_temple,
      control_chem_temple_total,
      user_id,
    };

    const rs: any = await surveilModel.savePrevalence(db, data);
    const prevalenceId = rs[0];

    // update prevalence house
    await surveilModel.updatePrevalanceRadius(db, period_day, prevalenceId);
    let imageData = [];

    arrImages.forEach((image: any) => {
      const obj: any = {};
      obj.prevalence_id = prevalenceId;
      obj.house_image = image;

      imageData.push(obj);
    });

    await surveilModel.savePrevalenceImage(db, imageData);

    res.send({ ok: true });
  } catch (error) {
    console.log(error);
    res.send({ ok: false, error: 'เกิดข้อผิดพลาด [500]' })
  }
});

router.delete('/line-info', async (req: Request, res: Response) => {
  const userId = req.decoded.id;
  const db: any = req.db;

  try {
    await userModel.clearLineInfo(db, userId);
    res.send({ ok: true });
  } catch (error) {
    console.log(error);
    res.send({ ok: false, error: 'เกิดข้อผิดพลาด [500]' })
  }
});
// /surveil/
router.post('/', async (req: Request, res: Response) => {
  const db: any = req.db;
  const patientInfo: any = req.body.patientInfo;
  const surveil: any = req.body.surveil;
  const labs: any = req.body.labs;
  const screen: any = req.body.screen;

  const _patientInfo: any = {};
  _patientInfo.hospcode = patientInfo.hospcode;
  _patientInfo.cid = patientInfo.cid;
  _patientInfo.hn = patientInfo.hn;
  _patientInfo.pt_name = patientInfo.pt_name;
  _patientInfo.sex = patientInfo.sex;
  _patientInfo.birthday = patientInfo.birthday;
  _patientInfo.address = patientInfo.address;
  _patientInfo.addrpart = patientInfo.addrpart;
  _patientInfo.moopart = patientInfo.moopart;
  _patientInfo.tmbpart = patientInfo.tmbpart;
  _patientInfo.amppart = patientInfo.amppart;
  _patientInfo.chwpart = patientInfo.chwpart;
  _patientInfo.tel = patientInfo.tel;
  _patientInfo.user_id_import = req.decoded.id;

  const _surveil: any = {};


  const rsAmp: any = await surveilModel.getAmpMasterCup(db, surveil.hospcode);
  const ampCode = rsAmp.length > 0 ? rsAmp[0].hamp : '00054';

  _surveil.hospcode = surveil.hospcode;

  _surveil.from = surveil.hospcode;
  _surveil.to = ampCode;

  _surveil.hn = surveil.hn;
  _surveil.sv_number = surveil.sv_number;
  _surveil.vn = surveil.vn;
  _surveil.pdx = surveil.pdx;
  _surveil.vstdate = surveil.vstdate;
  _surveil.report_date = surveil.report_date;
  _surveil.begin_date = surveil.begin_date;
  _surveil.icd_name = surveil.icd_name;
  _surveil.name506 = surveil.name506;
  _surveil.code506 = surveil.code506;
  _surveil.cc = surveil.cc;
  _surveil.pe = surveil.pe;
  _surveil.ill_addr = surveil.ill_addr;
  _surveil.ill_moo = surveil.ill_moo;
  _surveil.ill_tmbpart = surveil.ill_tmbpart;
  _surveil.ill_amppart = surveil.ill_amppart;
  _surveil.ill_chwpart = surveil.ill_chwpart;
  _surveil.diag_dangue = surveil.diag_dangue;
  _surveil.time_send = moment().format('YYYY-MM-DD HH:mm:ss');
  _surveil.user_id = req.decoded.id;

  try {
    const rsDup: any = await surveilModel.checkDuplicatedPatient(db, _patientInfo.hospcode, _patientInfo.hn);
    if (rsDup.length) {
      const _info: any = {};
      _info.pt_name = patientInfo.pt_name;
      _info.sex = patientInfo.sex;
      _info.birthday = patientInfo.birthday;
      _info.address = patientInfo.address;
      _info.addrpart = patientInfo.addrpart;
      _info.moopart = patientInfo.moopart;
      _info.tmbpart = patientInfo.tmbpart;
      _info.amppart = patientInfo.amppart;
      _info.chwpart = patientInfo.chwpart;
      _info.tel = patientInfo.tel;
      _info.user_id_import = req.decoded.id;

      await surveilModel.updatePatientInfo(db, _patientInfo.hospcode, _patientInfo.hn, _info)
    } else {
      await surveilModel.savePatientInfo(db, _patientInfo);
    }

    // surveil
    let surveilId: any;

    const rsSurveilDup: any = await surveilModel.checkDuplicatedSurveil(db, _surveil.hospcode, _surveil.hn, _surveil.sv_number);
    if (rsSurveilDup.length) {

      surveilId = rsSurveilDup[0].surveil_id;

      const _surveilUpdate: any = {};
      _surveilUpdate.vn = surveil.vn;
      _surveilUpdate.pdx = surveil.pdx;
      _surveilUpdate.vstdate = surveil.vstdate;
      _surveilUpdate.report_date = surveil.report_date;
      _surveilUpdate.begin_date = surveil.begin_date;
      _surveilUpdate.icd_name = surveil.icd_name;
      _surveilUpdate.name506 = surveil.name506;
      _surveilUpdate.code506 = surveil.code506;
      _surveilUpdate.cc = surveil.cc;
      _surveilUpdate.pe = surveil.pe;
      _surveilUpdate.ill_addr = surveil.ill_addr;
      _surveilUpdate.ill_moo = surveil.ill_moo;
      _surveilUpdate.ill_tmbpart = surveil.ill_tmbpart;
      _surveilUpdate.ill_amppart = surveil.ill_amppart;
      _surveilUpdate.ill_chwpart = surveil.ill_chwpart;
      _surveilUpdate.diag_dangue = surveil.diag_dangue;
      _surveilUpdate.time_send = moment().format('YYYY-MM-DD HH:mm:ss');
      _surveilUpdate.user_id = req.decoded.id;

      await surveilModel.updateSurveil(db, _surveil.hospcode, _surveil.hn, _surveil.sv_number, _surveilUpdate);

    } else {
      const rsSurveilId = await surveilModel.saveSurveil(db, _surveil);
      surveilId = rsSurveilId[0];
    }

    // lab
    const labItems: any = [];

    if (labs.length) {
      labs.forEach(v => {
        const obj: any = {};
        obj.surveil_id = surveilId;
        obj.hospcode = surveil.hospcode;
        obj.hn = v.hn;
        obj.vn = v.vn;
        obj.lab_order_number = v.lab_order_number;
        obj.order_date = v.order_date;
        obj.order_time = v.order_time;
        obj.lab_items_name_ref = v.lab_items_name_ref;
        obj.lab_order_result = v.lab_order_result;
        obj.lab_items_normal_value = v.lab_items_normal_value;
        obj.lab_items_unit = v.lab_items_unit;

        labItems.push(obj);
      });
      // remove
      await surveilModel.removeLab(db, surveilId);
      // save
      await surveilModel.saveLab(db, labItems);
    }

    // screen
    const _screen: any = {};

    const rsScreenDup: any = await surveilModel.checkDuplicatedScreen(db, surveilId);

    _screen.sv_number = screen.sv_number;
    _screen.vn = screen.vn;
    _screen.screen_fever = screen.screen_fever;
    _screen.screen_headache = screen.screen_headache;
    _screen.screen_eye = screen.screen_eye;
    _screen.screen_muscle = screen.screen_muscle;
    _screen.screen_ortho = screen.screen_ortho;
    _screen.screen_rash = screen.screen_rash;
    _screen.screen_skin = screen.screen_skin;
    _screen.screen_liver = screen.screen_liver;
    _screen.screen_shock = screen.screen_shock;
    _screen.screen_tourniquet = screen.screen_tourniquet;

    if (rsScreenDup.length) {
      await surveilModel.updateScreen(db, surveilId, _screen);
    } else {
      _screen.surveil_id = surveilId;
      await surveilModel.saveScreen(db, _screen);
    }

    res.send({ ok: true, });

  } catch (error) {
    console.log(error);
    res.send({ ok: false, error: 'เกิดข้อผิดพลาด [500]' });
  }
});

router.post('/send', async (req: Request, res: Response) => {
  const db: any = req.db;
  const userId = req.decoded.id;
  const hospcode = req.decoded.hospcode;
  const surveilId = req.body.surveilId;
  const areaCode = req.decoded.areaCode;
  const illAmppart = req.body.illAmppart;

  let responseHospcode = '00000';
  let ssjHospcode = '00054';

  let lineIds = [];

  if (illAmppart === areaCode) {
    // ดึงข้อมูล สสอ.
    const rsAmp: any = await userModel.checkMasterCode(db, hospcode);

    if (rsAmp.length) {
      responseHospcode = rsAmp[0].hamp;
      const rsAmpLine: any = await userModel.getLineIds(db, responseHospcode);
      rsAmpLine.forEach(v => {
        if (v.id_line) {
          lineIds.push({ id: v.id, idLine: v.id_line, hospcode: v.hospcode });
        }
      });

      const rsSSJLine: any = await userModel.getLineIds(db, ssjHospcode);
      rsSSJLine.forEach(v => {
        if (v.id_line) {
          lineIds.push({ id: v.id, idLine: v.id_line, hospcode: v.hospcode });
        }
      });
    }
  } else {
    const rsSSJLine: any = await userModel.getLineIds(db, ssjHospcode);
    rsSSJLine.forEach(v => {
      if (v.id_line) {
        lineIds.push({ id: v.id, idLine: v.id_line, hospcode: v.hospcode });
      }
    });
  }

  // patient info
  const rsInfo: any = await surveilModel.getSurveilInfo(db, surveilId);
  const info = rsInfo.length ? rsInfo[0] : {};

  for (const v of lineIds) {
    const message = {
      "type": "flex",
      "altText": "แจ้งข้อมูลระบาด",
      "contents": {
        "type": "bubble",
        "body": {
          "type": "box",
          "layout": "vertical",
          "contents": [
            {
              "type": "text",
              "text": info.hospname,
              "wrap": true,
              "weight": "bold"
            },
            {
              "type": "text",
              "text": `ชื่อผู้ป่วย ${info.pt_name}`,
              "wrap": true
            },
            {
              "type": "text",
              "text": `โรคทางระบาด: ${info.name506}`,
              "wrap": true
            }
          ]
        },
        "footer": {
          "type": "box",
          "layout": "horizontal",
          "contents": [
            {
              "type": "button",
              "style": "primary",
              "action": {
                "type": "uri",
                "label": "รับทราบ",
                "uri": `${process.env.API_LINK}/line/accept?idLine=${v.idLine}&hospcode=${v.hospcode}&userId=${v.id}&surveilId=${surveilId}`
              }
            }
          ]
        }
      }
    };
    lineModel.pushMessage(v.idLine, [message]);
  }

  // save log

  for (const item of lineIds) {
    const sendDate = moment().format('YYYY-MM-DD HH:mm:ss');
    await surveilModel.saveSendLog(db, surveilId, userId, hospcode, item.hospcode, sendDate);
  }

  // console.log(lineIds);
  // console.log(JSON.stringify(message));

  res.send({ ok: true });
});

router.put('/', async (req: Request, res: Response) => {

});

router.delete('/', async (req: Request, res: Response) => {

});

export default router;
