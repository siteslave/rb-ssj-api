import * as knex from 'knex';

export class SurveilModel {

  saveSurveil(db: knex, surveil: any) {
    return db('patient_surveil')
      .insert(surveil, 'surveil_id');
  }

  changeSurveilStatus(db: knex, surveilId: any, status: any) {
    return db('patient_surveil')
      .where('surveil_id', surveilId)
      .update('status_id', status);
  }

  saveLab(db: knex, labs: any[]) {
    return db('patient_lab')
      .insert(labs);
  }

  saveScreen(db: knex, screen: any) {
    return db('patient_screen')
      .insert(screen);
  }

  updateScreen(db: knex, surveilId: any, screen: any) {
    return db('patient_screen')
      .where('surveil_id', surveilId)
      .update(screen);
  }

  checkDuplicatedScreen(db: knex, surveilId: any) {
    return db('patient_screen')
      .where('surveil_id', surveilId);
  }

  removeLab(db: knex, surveilId: any) {
    return db('patient_lab')
      .where('surveil_id', surveilId)
      .del();
  }

  updateSurveil(db: knex, hospcode: any, hn: any, svNumber: any, surveil: any) {
    return db('patient_surveil')
      .where('hospcode', hospcode)
      .where('sv_number', svNumber)
      .where('hn', hn)
      .update(surveil);
  }

  checkDuplicatedSurveil(db: knex, hospcode: any, hn: any, svNumber: any) {
    return db('patient_surveil')
      .select('surveil_id')
      .where('hospcode', hospcode)
      .where('sv_number', svNumber)
      .where('hn', hn);
  }

  savePatientInfo(db: knex, patient: any) {
    return db('patient_info')
      .insert(patient);
  }

  updatePatientInfo(db: knex, hospcode: any, hn: any, patient: any) {
    return db('patient_info')
      .where('hospcode', hospcode)
      .where('hn', hn)
      .update(patient);
  }

  checkDuplicatedPatient(db: knex, hospcode: any, hn: any) {
    return db('patient_info')
      .where('hospcode', hospcode)
      .where('hn', hn);
  }

  getReadySend(db: knex, hospcode: any) {
    return db('patient_surveil')
      .select('sv_number')
      .where('hospcode', hospcode);
  }

  getHistory(db: knex, hospcode: any, query: any, limit: number, offset: any) {

    const sql = db('surveil_log as l')
      .select(db.raw('count(*) as total'))
      .whereRaw('l.surveil_id=ps.surveil_id')
      .as('send_log');

    let querySql = db('patient_surveil as ps')
      .select('ps.*', 'p.pt_name', 'p.birthday', 'p.cid', 'p.sex', 'p.address', sql)
      .joinRaw('inner join patient_info as p on p.hospcode=ps.hospcode and p.hn=ps.hn')
      .where('ps.hospcode', hospcode);

    if (query) {
      const _query = `%${query}%`;

      querySql.where(w => w
        .where('p.hn', 'like', _query)
        .orWhere('p.pt_name', 'like', _query)
        .orWhere('p.cid', query));
    }

    return querySql.orderBy('ps.vstdate', 'desc')
      .limit(limit).offset(offset);
  }

  async getHistoryTotal(db: knex, hospcode: any, query: any) {

    let querySql = db('patient_surveil as ps')
      .select(db.raw('count(*) as total'))
      .joinRaw('inner join patient_info as p on p.hospcode=ps.hospcode and p.hn=ps.hn')
      .where('ps.hospcode', hospcode);

    if (query) {
      const _query = `%${query}%`;

      querySql.where(w => w
        .where('p.hn', 'like', _query)
        .orWhere('p.pt_name', 'like', _query)
        .orWhere('p.cid', query));
    }

    const rs: any = await querySql;

    return +rs[0].total || 0;
  }

  getHistoryList(db: knex, hospcode: any, query: any) {

    const sql = db('surveil_log')
      .select('surveil_id');

    const sqlCount = db('prevalence as pl')
      .select(db.raw('count(*) as total'))
      .whereRaw('pl.surveil_id=ps.surveil_id')
      .as('total');

    let querySql = db('patient_surveil as ps')
      .select('ps.*', 'p.pt_name', 'p.birthday', 'p.cid', 'p.sex', 'p.address', sqlCount)
      .joinRaw('inner join patient_info as p on p.hospcode=ps.hospcode and p.hn=ps.hn')
      .where('ps.to', hospcode)
      .whereNull('ps.status_id');

    if (query) {
      const _query = `%${query}%`;

      querySql.where(w => w
        .where('p.hn', 'like', _query)
        .orWhere('p.pt_name', 'like', _query)
        .orWhere('p.cid', query));
    }

    return querySql.orderBy('ps.vstdate', 'desc')
      .limit(100);
  }

  getJobHistoryList(db: knex, hospcode: any, query: any) {

    const sqlCount = db('prevalence as pl')
      .select(db.raw('count(*) as total'))
      .whereRaw('pl.surveil_id=ps.surveil_id')
      .as('total');

    let querySql = db('patient_surveil as ps')
      .select('ps.*', 'p.pt_name', 'p.birthday', 's.status_name as status', 'p.cid', 'p.sex', 'p.address', sqlCount)
      .joinRaw('inner join patient_info as p on p.hospcode=ps.hospcode and p.hn=ps.hn')
      .leftJoin('status as s', 's.status_id', 'ps.status_id')
      .where('ps.to', hospcode)
      .whereIn('ps.status_id', ['D', 'C', 'Z']);

    if (query) {
      const _query = `%${query}%`;

      querySql.where(w => w
        .where('p.hn', 'like', _query)
        .orWhere('p.pt_name', 'like', _query)
        .orWhere('p.cid', query));
    }

    return querySql
      .orderByRaw('ps.vstdate desc, ps.surveil_id desc')
      .groupBy('ps.sv_number');
  }

  getHistoryListByAmpur(db: knex, toHospcode: any, query: any) {

    const sql = db('surveil_log')
      .select('surveil_id');

    const sqlCount = db('prevalence as pl')
      .select(db.raw('count(*) as total'))
      .whereRaw('pl.surveil_id=ps.surveil_id')
      .as('total');

    let querySql = db('patient_surveil as ps')
      .select('ps.*', 'p.pt_name', 'p.birthday', 'p.cid', 'p.sex', 'p.address', sqlCount)
      .joinRaw('inner join patient_info as p on p.hospcode=ps.hospcode and p.hn=ps.hn')
      .whereIn('ps.surveil_id', sql);


    if (query) {
      const _query = `%${query}%`;

      querySql.where(w => w
        .where('p.hn', 'like', _query)
        .orWhere('p.pt_name', 'like', _query)
        .orWhere('p.cid', query));
    }

    return querySql
      .where('ps.to', toHospcode)
      .whereNull('ps.status_id')
      .orderBy('ps.vstdate', 'desc')
      .limit(100);
  }

  getSurveilInfo(db: knex, surveilId: any) {
    return db('patient_surveil as ps')
      .select('ps.*', 'p.pt_name', 'p.birthday', 'p.cid', 'p.sex', 'mh.hmainname as hospname', 'mh.hamp')
      .joinRaw('inner join patient_info as p on p.hospcode=ps.hospcode and p.hn=ps.hn')
      .leftJoin('master_hospcode as mh', 'mh.hmain', 'ps.hospcode')
      .where('ps.surveil_id', surveilId)
      .limit(1);
  }

  getFirstDate(db: knex, surveil: any) {
    return db('patient_surveil')
      .select('vstdate').where('surveil_id', surveil).limit(1);
  }

  getClients(db: knex, hamp: any) {
    return db('master_hospcode')
      .where('hamp', hamp)
      .where('hostype', 18);
  }

  getAmpMasterCup(db: knex, hospcode: any) {
    return db('master_hospcode')
      .select('hamp')
      .where('hospcode', hospcode)
      .limit(1);
  }

  getProvinceClients(db: knex, provinceCode: any) {
    return db('master_hospcode')
      .where('changwat', provinceCode)
      .where('hostype', '02');
  }

  saveSendLog(db: knex, surveilId: any,
    userId: any,
    fromHospcode: any,
    toHospcode: any,
    sendDate: any) {
    return db('surveil_log')
      .insert({
        surveil_id: surveilId,
        user_send: userId,
        send_from_hospcode: fromHospcode,
        send_to_hospcode: toHospcode,
        send_date: sendDate
      });
  }

  savePrevalence(db: knex, data: any) {
    return db('prevalence')
      .insert(data, 'prevalence_id');
  }

  savePrevalenceImage(db: knex, data: any) {
    return db('house_image')
      .insert(data);
  }

  savePrevalanceRadius(db: knex, data: any) {
    return db('prevalence_radius')
      .insert(data);
  }

  getPrevalanceRadius(db: knex, periodDay: any) {
    return db('prevalence_radius')
      .where('period_day', periodDay)
      .orderBy('address');
  }

  updatePrevalanceRadius(db: knex, periodDay: any, prevalenceId: any) {
    return db('prevalence_radius')
      .where('period_day', periodDay)
      .update('prevalence_id', prevalenceId);
  }

  removePrevalanceRadius(db: knex, prevalenceRadiusId: any) {
    return db('prevalence_radius')
      .where('prevalence_radius_id', prevalenceRadiusId)
      .del();
  }

  checkPrevalanceRadiusHouse(db: knex, surveilId: any, address: any) {
    return db('prevalence_radius')
      .where('surveil_id', surveilId)
      .where('address', address);
  }

  async countPrevalence(db: knex, surveilId: any) {
    const rs: any = await db('prevalence').select(db.raw('count(*) as total')).where('surveil_id', surveilId);
    return +rs[0].total;
  }

  checkReplyStatus(db: knex, surveilId: any, hospcode: any) {
    return db('surveil_log as l')
      .select('l.*', 'u.name as user_fullname')
      .innerJoin('users as u', 'u.id', 'l.respond_user')
      .where('l.send_to_hospcode', hospcode)
      .where('l.surveil_id', surveilId)
      .where('l.status', 'Y');
  }

  checkSendLogInfo(db: knex, surveilId: any) {
    return db('surveil_log as l')
      .select('l.*', 'pi.pt_name', 'pt.name506')
      .innerJoin('patient_surveil as pt', 'pt.surveil_id', 'l.surveil_id')
      .joinRaw('inner join patient_info as pi on pi.hn=pt.hn and pi.hospcode=pt.hospcode')
      .where('l.surveil_id', surveilId);
  }

  updateReplyStatus(db: knex, surveilId: any, status: any,
    hospcode: any, responseUserId: any, responseTime: any) {
    return db('surveil_log')
      .where('surveil_id', surveilId)
      // .where('send_to_hospcode', hospcode)
      .update({
        status: status,
        respond_user: responseUserId,
        respond_time: responseTime
      })
  }

}