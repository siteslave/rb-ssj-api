const request = require("request");

export class LineModel {

  sendNotify(message) {
    var options = {
      method: 'POST',
      url: 'https://notify-api.line.me/api/notify',
      headers:
      {
        'Postman-Token': 'da447963-1647-4dd7-8ce9-8b659a8be7ea',
        'cache-control': 'no-cache',
        'Authorization': `Bearer ${process.env.LINE_NOTIFY_TOKEN}`,
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      form:
      {
        message: message
      }
    };

    request(options, function (error, response, body) {
      if (error) throw new Error(error);

      console.log(body);
    });

  }

  replyMessage(replyToken: any, messages: any) {
    var options = {
      method: 'POST',
      url: 'https://api.line.me/v2/bot/message/reply',
      headers:
      {
        'cache-control': 'no-cache',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${process.env.LINE_BOT_TOKEN}`,
      },
      body:
      {
        replyToken: replyToken,
        messages: messages
      },
      json: true
    };

    request(options, function (error, response, body) {
      if (error) throw new Error(error);

      console.log(body);
    });
  }

  pushMessage(userId, messages) {
    var options = {
      method: 'POST',
      url: 'https://api.line.me/v2/bot/message/push',
      headers:
      {
        'cache-control': 'no-cache',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${process.env.LINE_BOT_TOKEN}`,
      },
      body:
      {
        to: userId,
        messages: messages
      },
      json: true
    };

    request(options, function (error, response, body) {
      if (error) throw new Error(error);

      console.log(body);
    });
  }

  verifyAccessToken(token: any) {
    return new Promise((resolve: any, reject: any) => {
      var options = {
        method: 'GET',
        url: 'https://api.line.me/oauth2/v2.1/verify',
        qs: {
          access_token: token,
        },
        headers: { 'content-type': 'application/x-www-form-urlencoded' },
      };

      request(options, function (error, response, body) {
        if (error) reject(error);
        resolve(body);
      });
    });
  }

}