import * as knex from 'knex';

export class UserModel {

  doLogin(db: knex, username: any, password: any) {
    return db('users')
      .select('id', 'name', 'level', 'hospcode', 'amphur')
      .where('login_name', username)
      .where('login_password', password);
  }

  saveLineId(db: knex, username: any, lineId: any) {
    return db('users')
      .update('id_line', lineId)
      .where('login_name', username);
  }

  getInfo(db: knex, userId: any) {
    return db('users as u')
      .select('u.name', 'u.id_line', 'u.hospcode',
        'u.line_email', 'u.line_name', 'u.line_picture_url', 'mh.hsubname as hospname')
      .leftJoin('master_hospcode as mh', 'mh.hsub', 'u.hospcode')
      .where('u.id', userId)
  }

  // save from app
  saveLineInfo(db: knex, userId: any, lineId: any, lineName: any, lineEmail: any, linePictureUrl: any) {
    return db('users')
      .update('id_line', lineId)
      .update('line_name', lineName)
      .update('line_email', lineEmail)
      .update('line_picture_url', linePictureUrl)
      .where('id', userId);
  }
  // save from app
  clearLineInfo(db: knex, userId: any) {
    return db('users')
      .update('id_line', null)
      .update('line_name', null)
      .update('line_email', null)
      .update('line_picture_url', null)
      .where('id', userId);
  }

  checkRegisteredLine(db: knex, lineId: any) {
    return db('users')
      .where('id_line', lineId);
  }

  getLineIds(db: knex, hospcode: any) {
    return db('users')
      .select('id_line', 'id', 'hospcode')
      .where('hospcode', hospcode);
  }

  getUserInfo(db: knex, userId: any) {
    return db('users')
      .select('id_line', 'id', 'hospcode')
      .where('id', userId);
  }

  checkMasterCode(db: knex, hospcode: any) {
    return db('master_hospcode')
      .where('hmain', hospcode);
  }
}